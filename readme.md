[fr]

Ce jeu est adapté d'un de mes projets d'école. Si vous réutilisez ce code, merci de me citer.

Une version de démonstration est disponible à https://josephchataignon.github.io/Demos/space-invaders.html

Le fichier à ouvrir pour lancer le jeu est "code.html".

Il s'agit d'un fichier HTML, contenant tout le code HTML et Javascript du projet.
Seul le curseur personnalisé est introduit par le code CSS placé dans le fichier "style.css".

Le dossier "Ressources" contient les images et les fichiers audio nécessaires au jeu.

[en]

This game is adapted from one of my school projects. If you use this code, please credit me.

A demo version is availale at https://josephchataignon.github.io/Demos/space-invaders.html

The file to open for launching the game is "code.html".

The file "Ressources" contains all the images and audio files necessary to the game.

[it]

Questo gioco è adattato da uno dei miei progetti di scuola. Se reutilizzi il codice, menzionami per favore.

Una versione demo è disponibile su https://josephchataignon.github.io/Demos/space-invaders.html

Il file da aprire per lanciare il gioco è "code.html".

Il file "Ressources" contiene tutte le immagini e audio file necessari al gioco.

--

Joseph CHATAIGNON
